// MyTime.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include <iostream>
#include "my_time_3.h"

//int _tmain(int argc, _TCHAR* argv[])
int main(int argc, char * argv[])
{
	using std::cout;
	using std::endl;
	Time aida(3, 35);
	Time tosca(2, 48);
	Time temp;
	cout << "Aida and Tosca:\n";
	cout << aida << "; " << tosca << endl;
	temp = aida + tosca; // operator+()
	cout << "Aida + Tosca: " << temp << endl;
	temp = aida* 1.17; // member operator*()
	cout << "Aida * 1.17: " << temp << endl;
	cout << "10.0 * Tosca: " << 10.0 * tosca << endl;

	while(1);
	return 0;
}


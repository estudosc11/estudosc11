// ConsoleApplication.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Control.h"

int _tmain(int argc, _TCHAR* argv[])
{
	using namespace std;
	Control control;
	control.Start();
	std::cout << "Digite um nome para sair:" << std::endl;
	std::string name;
	std::getline(std::cin, name);
	return 0;
}


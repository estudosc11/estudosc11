#pragma once

const int Max_Block = 10;

class BlockRTP
{
	char * blocks[Max_Block];
	unsigned int block;
	unsigned int block_show;
public:
	BlockRTP();
	~BlockRTP();
	bool Pop();
	bool Pop(bool free_flg);
	bool Push(char * blk);

};


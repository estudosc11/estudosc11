#include "stdafx.h"
#include "BlockRTP.h"
#include <cstdlib>
#include <iostream>

BlockRTP::BlockRTP()// : block(0), block_show(0)
{
	block = 0;
	block_show = 0;
}


BlockRTP::~BlockRTP()
{

}

bool BlockRTP::Pop()
{
	using namespace std;
	if (block == 0)
		return false;
	if (blocks[0] != NULL)
		free(blocks[0]);
	for (int i = 0; i < block-1; i++)
	{
		blocks[i] = blocks[i + 1];
	}
	cout << "Pop block " << block << "\n";
	block--;
	return true;
}

bool BlockRTP::Pop(bool free_flg)
{
	using namespace std;
	if (block == 0)
		return false;
	if (blocks[0] != NULL)
	if (free_flg == true)
		free(blocks[0]);
	for (int i = 0; i < block - 1; i++)
	{
		blocks[i] = blocks[i + 1];
	}
	cout << "Pop block without free " << block << "\n";
	block--;
	return true;
}

bool BlockRTP::Push(char * blk)
{
	using namespace std;
	if (block + 1 == Max_Block)
		Pop();
	cout << "Push block " << block << "\n";
	blocks[block++] = blk;
	return true;
}


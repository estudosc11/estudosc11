#pragma once

#include <iostream>
#include <thread>
#include <string>
#include <cstdlib>
#include "BlockRTP.h"
#include "ReceiveRTP.h"
#include "Screen.h"

class Control
{
public:
	BlockRTP blockRTP;
	ReceiveRTP receiveRTP;
	Screen screen;
	Control();
	~Control();
	void Start();
};


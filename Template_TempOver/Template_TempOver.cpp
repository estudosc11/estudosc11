// Template_TempOver.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include <iostream>

template <typename T> // template A
void ShowArray(T arr[], int n);

template <typename T> // template B
void ShowArray(T * arr[], int n);

struct debts
{
	char name[50];
	double amount;
};

int main(int argc, char * argv[])
{
	using namespace std;
	int things[6] = { 13, 31, 103, 301, 310, 130 };
	struct debts mr_E[3] =
	{
		{ "Ima Wolf", 2400.00 },
		{ "Ura Foxe", 1300.00 },
		{ "Iby Sout", 1800.00 }
	};

	double *pd[3];
	// set pointers to the amount members to the structures in mr_E
	for (int i = 0; i < 3; i++)
		pd[i] = &mr_E[i].amount;
	cout << "Listing Mr E's counts of things: \n";
	// things is an array of int
	ShowArray(things, 6); // uses template A
	cout << "Listing Mr E's debts:\n";
	ShowArray(pd, 3); // uses templabe B (more specialized)
	
	while (1) ;
	return 0;
}

template <typename T>
void ShowArray(T arr[], int n)
{
	using namespace std;
	cout << "template A\n";
	for (int i = 0; i < n; i++)
		cout << arr[i] << ' ';
	cout << endl;
}

template <typename T>
void ShowArray(T * arr[], int n)
{
	using namespace std;
	cout << "template B\n";
	for (int i = 0; i < n; i++)
		cout << *arr[i] << ' ';
	cout << endl;
}

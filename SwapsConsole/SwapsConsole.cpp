// SwapsConsole.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>

void swapr(int & a, int & b); // a, b are aliases for ints
void swapp(int * p, int * q); // q, q are address of ints
void swapv(int a, int b); // a, b are new variables

int _tmain(int argc, _TCHAR* argv[])
{
	using namespace std;
	int wallet1 = 300;
	int wallet2 = 350;
	cout << "wallet1 = $" << wallet1;
	cout << ", wallet2 = $" << wallet2 << endl;

	cout << "Using references to swap contents:\n";
	swapr(wallet1, wallet2); // pass variables
	cout << "wallet1 = $" << wallet1;
	cout << ", wallet2 = $" << wallet2 << endl;

	cout << "Using ponters to swap contents again:\n";
	swapp(&wallet1, &wallet2); // pass variables
	cout << "wallet1 = $" << wallet1;
	cout << ", wallet2 = $" << wallet2 << endl;

	cout << "Trying to use passing to value:\n";
	swapv(wallet1, wallet2); // pass variables
	cout << "wallet1 = $" << wallet1;
	cout << ", wallet2 = $" << wallet2 << endl;

	return 0;
}

void swapr(int & a, int & b) // use references
{
	int temp;
	temp = a; // use a, b for values of variables
	a = b;
	b = temp;
}

void swapp(int * p, int * q) // use pointers
{
	int temp;
	temp = *p; // use *p, *q for values of variables
	*p = *q;
	*q = temp;
}

void swapv(int a, int b) // try using variables
{
	int temp;
	temp = a; // use a, b for values of variables
	a = b;
	b = temp;
}

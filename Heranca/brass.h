// brass.h -- bank account classes

#ifndef BRASS_H_
#define BRASS_H_

#include <string>

// Brass Account Class
class Brass
{
private:
	std::string fullName;
	long acctNum;
	double balance;
public:
	Brass(const std::string & s = "Nullbody", long an = 1,
		double bal = 0.0);
	void Deposit(double amt);
	double Balance() const;
	// metodos virtuais para redefinir na classe derivada
	virtual void Withdraw(double amt);
	virtual void ViewAcct() const;
	virtual ~Brass() {};
};

// Brass Plus Account Class

class BrassPlus : public Brass
{
private:
	double maxLoan; // emprestimo
	double rate; // taxa
	double owesBank;
public:
	BrassPlus(const std::string & s = "Nullbody", long an = 1,
		double bal = 0.0, double ml = 500,
		double r = 0.11125);
	BrassPlus(const Brass & ba, double ml = 500, double r = 0.11125);
	void ResetMax(double m) { maxLoan = m; }
	void ResetRate(double r) { rate = r; }
	void ResetOwes() { owesBank = 0; }
	// metodos virtuais redefinidos
	virtual void Withdraw(double amt);
	virtual void ViewAcct() const;
};

#endif

// Listing 13.12

// Bank account class methods

#include <iostream>
#include <string>
#include "acctabc.h"

using std::cout;
using std::ios_base;
using std::endl;
using std::string;

// Abstract Base Class

AcctABC::AcctABC(const string & s, long am, double bal)
{
	fullName = s;
	acctNum = am;
	balance = bal;
}

void AcctABC::Deposit(double amt)
{
	if (amt < 0)
	{
		cout << "Negative deposit not allowed: "
			<< "deposit is cancelled.\n";
	}
	else
		balance += amt;
}

void AcctABC::Withdraw(double amt)
{
	balance -= amt;
}

// Protected Methods for Formating

AcctABC::Formating AcctABC::setFormat() const
{
	// Set up ###.## format
	Formating f;
	f.flag = cout.setf(ios_base::fixed, ios_base::floatfield);
	return f;
}

void AcctABC::Restore(Formating & f) const
{
	cout.setf(f.flag, ios_base::floatfield);
	cout.precision(f.pr);
}

// Brass Methods

void Brass::Withdraw(double amt)
{
	if (amt < 0)
		cout << "Withdrawal amount must be positive: "
		<< "Withdrawal canceled.\n";
	else if (amt <= Balance())
		AcctABC::Withdraw(amt);
	else
		cout << "Withdrawal amount of $" << amt
		<< "exceeds your balance.\n"
		<< "Withdrawal canceled.\n";
}

void Brass::ViewAcct() const
{
	Formating f = setFormat();
	cout << "Brass Client: " << FullName() << endl;
	cout << "Account Number: " << AcctNum() << endl;
	cout << "Balance: $" << Balance() << endl;
	Restore(f);
}

// BrassPlus Methods

BrassPlus::BrassPlus(const string & s, long am, double bal,
	double ml, double r) : AcctABC(s, am, bal)
{
	maxLoan = ml;
	owesBank = 0.0;
	rate = r;
}

BrassPlus::BrassPlus(const Brass & ba, double ml, double r)
	: AcctABC(ba) // Uses implicit copy constructor
{
	maxLoan = ml;
	owesBank = 0.0;
	rate = r;
}

void BrassPlus::ViewAcct() const
{
	Formating f = setFormat();
	cout << "BrassPlus Client: " << FullName() << endl;
	cout << "Account Number: " << AcctNum() << endl;
	cout << "Balance: $" << Balance() << endl;
	cout << "Maximum Loan: " << owesBank << endl;
	cout.precision(3);
	cout << "Loan Rate: " << 100 * rate << "%\n";
	Restore(f);
}

void BrassPlus::Withdraw(double amt)
{
	Formating f = setFormat();
	double bal = Balance();
	if (amt <= bal)
	{
		AcctABC::Withdraw(amt);
	}
	else if (amt <= bal + maxLoan - owesBank)
	{
		double advance = amt - bal;
		owesBank += advance * (1.0 + rate);
		cout << "Bank advance: $" << advance << endl;
		cout << "Finance charge: $" << advance * rate << endl;
		Deposit(advance);
		AcctABC::Withdraw(amt);
	}
	else
		cout << "Credit limit exceeded. Transaction canceled.\n";
	Restore(f);
}


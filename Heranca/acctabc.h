// Applying the ABC - Abstract Base Class - concept

// Listing 13.11 - acctabc.h
// Bank Account Classes

#ifndef ACCTABC_H_
#define ACCTABC_H_

#include <iostream>
#include <string>

// Abstract Base Class
///////////////////////////////////////////////////////////////////////////
class AcctABC
{
private:
	std::string fullName;
	long acctNum;
	double balance;
protected:
	struct Formating
	{
		std::ios_base::fmtflags flag;
		std::streamsize pr;
	};
	const std::string & FullName() const { return fullName; }
	long AcctNum() const { return acctNum; }
	Formating setFormat() const;
	void Restore(Formating & f) const;
public:
	AcctABC(const std::string & s = "NullBody", long an = -1,
		double bal = 0.0);
	void Deposit(double ant);
	virtual void Withdraw(double ant) = 0; // pure virtual function
	double Balance() const { return balance; }
	virtual void ViewAcct() const = 0; // pure virtual function
	virtual ~AcctABC() { }
};


// Brass Account Class
///////////////////////////////////////////////////////////////////////////

class Brass : public AcctABC
{
public:
	Brass(const std::string & s = "NullBody", long an = -1,
		double bal = 0.0) : AcctABC(s, an, bal) {}
	virtual void Withdraw(double amt);
	virtual void ViewAcct() const;
	virtual ~Brass() { }
};


// Brass Plus Account Class
///////////////////////////////////////////////////////////////////////////

class BrassPlus : public AcctABC
{
private:
	double maxLoan;
	double rate;
	double owesBank;
public:
	BrassPlus(const std::string & s = "NullBody", long an = -1,
		double bal = 0.0, double ml = 500,
		double r = 0.0);
	BrassPlus(const Brass & ba, double ml = 500, double r = 0.1);
	virtual void ViewAcct() const;
	virtual void Withdraw(double amt);
	void ResetMax(double m) { maxLoan = m; }
	void ResetRate(double r) { rate = r; }
	void ResetOwes() { owesBank = 0; }
};

#endif
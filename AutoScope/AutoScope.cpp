// AutoScope.cpp : Defines the entry point for the console application.
//

// ilustrating scope of automatic variables

//#include "stdafx.h"
#include <iostream>
void oil(int x);

int main(int argc, char * argv[])
{
	using namespace std;
	int texas = 31;
	int year = 2011;
	cout << "In main(), texas = " << texas << ", &texas = ";
	cout << &texas << endl;
	oil(texas);
	cout << "In main(), texas = " << texas << ", &texas = ";
	cout << &texas << endl;
	cout << "In main(), year = " << year << ", &year = ";
	cout << &year << endl;
	while (1);
	return 0;
}

void oil(int x)
{
	using namespace std;
	int texas = 5;
	cout << "In oil(), texas = " << texas << ", &texas = ";
	cout << &texas << endl;
	cout << "In oil(), x = " << texas << ", &x = ";
	cout << &x << endl;
	{// start block
		int texas = 113;
		cout << "In block{}, texas = " << texas << ", &texas = ";
		cout << &texas << endl;
		cout << "In block{}, x = " << x << ", &x = ";
		cout << &x << endl;
	}
	cout << "Post block texas = " << texas;
	cout << ", &texas = " << &texas << endl;
}


// Listing 13.15 dma.cpp

// DMA class methods

#include "dma.h"
#include <cstring>

// Base class methods

BaseDMA::BaseDMA(const char *l, int r)
{
	label = new char[std::strlen(l) + 1];
	std::strcpy(label, l);
	rating = r;
}

BaseDMA::BaseDMA(const BaseDMA & rs)
{
	label = new char[std::strlen(rs.label) + 1];
	std::strcpy(label, rs.label);
	rating = rs.rating;
}

BaseDMA::~BaseDMA()
{
	delete[] label;
}

BaseDMA & BaseDMA::operator=(const BaseDMA & rs)
{
	if (this == &rs)
		return *this;
	delete[] label;
	label = new char[std::strlen(rs.label) + 1];
	std::strcpy(label, rs.label);
	rating = rs.rating;
	return *this;
}

std::ostream & operator<<(std::ostream & os, const BaseDMA & rs)
{
	os << "Label: " << rs.label << std::endl;
	os << "Rating: " << rs.rating << std::endl;
	return os;
}

// LacksDMA methods

LacksDMA::LacksDMA(const char *c, const char *l, int r) : BaseDMA(l, r)
{
	std::strncpy(color, c, 39);
	color[39] = '\0';
}

LacksDMA::LacksDMA(const char *c, const BaseDMA & rs) : BaseDMA(rs)
{
	std::strncpy(color, c, COL_LEN - 1);
	color[COL_LEN - 1] = '\0';
}

std::ostream & operator << (std::ostream & os, const LacksDMA & ls)
{
	os << (const BaseDMA &) ls;
	os << "Color: " << ls.color << std::endl;
	return os;
}

// 
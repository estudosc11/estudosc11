// Listing 14.20 tempmemb.cpp
// template members
/*
#include <iostream>

using std::cout;
using std::endl;

template <typename T>
class beta
{
private:
	template <typename V> // nested template class member
	class hold
	{
	private:
		V val;
	public:
		hold(V v = 0) : val(v) {}
		void Show() const { cout << val << endl;  }
		V value() const { return val; }
	};
	hold<T> q; // template object
	hold<int> n; // template object;
public:
	beta(T t, int i) : q(t), n(i) {}
	template <typename U> // template method
	U blab(U v, T t) { return (n.value() * q.value()) * v / t; }
	void Show() const { q.Show(); n.Show(); }
};

int main()
{
	beta <double> guy(3.5, 3);
	cout << "T was set to double\n";
	guy.Show();
	cout << "V was set to T, which is double, then V was set to int\n";
	cout << guy.blab(10, 2.3) << endl;
	cout << "U was set to int\n";
	cout << guy.blab(10.0, 2.3) << endl;
	cout << "U was set to double\n";
	cout << "Done.\n";

	while (1);
	return 0;
}
*/
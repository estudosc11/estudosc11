// use external variables

// compile with external.cpp

#include <iostream>

extern double warming; // use warming from another file

// function prototypes
void update(double dt);
void local();

using std::cout;

void update(double dt) // modifies global variable
{
	extern double warming; // optional redeclaration
	warming += dt; // uses global warming
	cout << "Updating global warming to " << warming;
	cout << " degress.\n";
}

void local() // uses local variable
{
	double warming = 0.8; // new variable hides external one

	cout << "Local warming " << warming << " degress\n.";
	// access global variable
	// scope resolution operator
	cout << "But global warming = " << ::warming;
	cout << " degress.\n";
}


// external variables
// note: compile with support.cpp

#include <iostream>

using namespace std;

// external variable
double warming = 0.3; // warming defined

// function prototypes
void update(double dt);
void local();

int main() // uses global variables
{
	cout << "Global warming is " << warming << " degress.\n";
	update(0.1); // call function to charge warming
	cout << "Global warming is " << warming << " degress.\n";
	local(); // call function with local warming
	cout << "Global warming is " << warming << " degress.\n";
	while (1);
	return 0;
}

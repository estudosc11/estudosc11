// Listing 15.13 - new_except.cpp
// The bad_alloc exception

#include <iostream>
#include <new>
#include <cstdlib> // for exit(), EXIT_FAILURE

using namespace std;

struct Big
{
	double stuff[10000];
};

int main()
{
	Big * pb;
	Big * pc;
	Big * pd;
	try {
		cout << "Trying to get a big clock of memory.\n";
		//pb = new (std::nothrow) Big[20000]; // 1,600,000,000
		// return null pointer - fail allocation (old new form)
		pc = new Big[20000]; // 1,600,000,000 bytes
		pd = new Big[20000]; // 1,600,000,000 bytes
		cout << "Got past the new request:\n";
	}
	catch (bad_alloc & ba)
	{
		cout << "Caugth exception!\n";
		cout << ba.what() << endl;
		while (1);
		exit(EXIT_FAILURE);
	}
	cout << "Memory sucessfully allocated\n";
	pb[0].stuff[0] = 4;
	cout << pb[0].stuff[0] << endl;
	delete[] pb;

	while (1);
	return EXIT_SUCCESS;
}
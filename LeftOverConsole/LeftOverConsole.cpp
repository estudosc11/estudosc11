// LeftOverConsole.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"

#include <iostream>

unsigned long left(unsigned long num, unsigned ct);

char * left(const char * str, int n = 1);

template <class AnyType>
void mySwap(AnyType & a, AnyType & b)
{
	AnyType temp;
	temp = a;
	a = b;
	b = temp;
}

//int _tmain(int argc, _TCHAR* argv[])
int main(int argc, char * argv[])
{
	using namespace std;
	char * trip = "Hawaii!!"; // test value
	unsigned long n = 12345678; // test value
	int i;
	char * temp;
	for (i = 1; i < 10; i++)
	{
		cout << left(n, i) << endl;
		temp = left(trip, i);
		cout << temp << endl;
		delete [] temp;
	}
	int a = 10;
	int b = 20;
	cout << "a: " << a << endl;
	cout << "b: " << b << endl;
	mySwap(a, b);
	cout << "\n === Swap === \n" << endl;
	cout << "a: " << a << endl;
	cout << "b: " << b << endl;


	return 0;
}

// This function returns the first ct digits of the number num
unsigned long left(unsigned long num, unsigned ct)
{
	unsigned digits = 1;
	unsigned long n = num;
	if (ct == 0 || num == 0)
		return 0; // return 0 it nodigits
	while (n /= 10)
		digits++;
	if (digits > ct) 
	{
		ct = digits - ct;
		while (ct--)
			num /= 10;
		return num;
	} else return num;
}

// This function returns a pointer to an new string consisting of 
//the first n characters in the str string
char * left(const char * str, int n)
{
	if (n < 0) 
		n = 0;
	char *p = new char[n + 1];
	int i;
	for (i = 0; i < n && str[i]; i++)
		p[i] = str[i]; // copy characters
	while (i <= n)
		p[i++] = '\0'; // set rest of string to '\0'
	return p;

}

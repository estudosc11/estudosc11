// FirstRefConsole.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>

int _tmain(int argc, _TCHAR* argv[])
{
	using namespace std;
	int rats = 101;
	int &rodents = rats; // rodents is a reference
	cout << " rats = " << rats;
	cout << ", rodents = " << rodents << endl;
	rodents++;
	cout << " rats = " << rats;
	cout << ", rodents = " << rodents << endl;
// some implementation require type casting the following
// address to type unsigned
	cout << " rats address = " << &rats;
	cout << ", rodents address = " << &rodents << endl;


	return 0;
}


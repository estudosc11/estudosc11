// ConsoleDrawBitmap.cpp : Defines the entry point for the console application.
//
#include <stdio.h>
#include <windows.h>

#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"user32.lib")

char buf[1 << 22];
#include "stdafx.h"


int _tmain(int argc, _TCHAR* argv[])
{

	BITMAPFILEHEADER& bfh = (BITMAPFILEHEADER&)buf[0];
	BITMAPINFO& bi = (BITMAPINFO&)buf[sizeof(BITMAPFILEHEADER)];
	BITMAPINFOHEADER& bih = bi.bmiHeader;
	char* bitmap = &buf[bfh.bfOffBits];

	int WX = 1024, WY = 512; // window's width/height
	int SX = bih.biWidth, SY = bih.biHeight;

	HWND win = CreateWindow("STATIC", "Bitmap test", 0x90C0, 0, 0, WX, WY, 0, 0, GetModuleHandle(0), 0);

	MSG msg;
	PAINTSTRUCT ps;
	HDC DC = GetDC(win); // window's DC
	HBITMAP dib = CreateDIBitmap(DC, &bih, CBM_INIT, bitmap, &bi, DIB_RGB_COLORS);
	HDC dibDC = CreateCompatibleDC(DC); SelectObject(dibDC, dib);

	ShowWindow(win, SW_SHOWNOACTIVATE);
	SetFocus(win);

	while (GetMessage(&msg, win, 0, 0)) {
		int m = msg.message;
		if (m == WM_PAINT) {
			DC = BeginPaint(win, &ps);
			StretchBlt(DC, 0, 0, WX, WY, dibDC, 0, 0, SX, SY, SRCCOPY);
			EndPaint(win, &ps);
		}
		else if ((m == WM_KEYDOWN) || (m == WM_SYSKEYDOWN)) {
			break;
		}
		else {
			DispatchMessage(&msg);
		}
	}

	return 0;
}


// ConsoleRecur.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>

void countdown(int n);

int _tmain(int argc, _TCHAR* argv[])
{
	countdown(4); // call the recursive function
	return 0;
}

void countdown(int n)
{
	using namespace std;
	cout << "Counting down ... " << n << endl;
	if (n > 0) countdown(n - 1); // function calls itself
	cout << n << ": Kaboom!\n";
}

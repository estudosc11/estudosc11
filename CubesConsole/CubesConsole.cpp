// CubesConsole.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>

double cube(double a);
double refcube(double & ra);

int _tmain(int argc, _TCHAR* argv[])
{
	using namespace std;
	double x = 3.0;
	cout << cube(x);
	cout << " = cube of " << x << endl;
	cout << refcube(x);
	cout << " = cube of " << x << endl;
	return 0;
}

double cube(double a)
{
	a *= a * a;
	return a;
}

double refcube(double &ra)
{
	ra *= ra * ra;
	return ra;
}

// strc_refConsole.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>
#include <string>

struct free_throws {
	std::string name;
	int made;
	int attempts;
	float percent;
};

void display(const free_throws & ft);
void set_pc(free_throws & ft);
free_throws & accumulate(free_throws & target, const free_throws & source);

int _tmain(int argc, _TCHAR* argv[])
{
	using std::cout;
	// partial initializations remaining members set to 0
	free_throws one = { "Ifelsa Branch", 13, 14 };
	free_throws two = { "Andor Knott", 10, 16 };
	free_throws three = { "Minnie Max", 7, 9 };
	free_throws four = { "Whily Looper", 5, 9 };
	free_throws five = { "Long Long", 6, 14 };
	free_throws team = { "Throwgoods", 0, 0 };

	// no initialization
	free_throws dup;
	cout << "no initialization ----------------------------------\n";
	set_pc(one);
	display(one);
	accumulate(team, one);
	display(team);
	cout << "use return value as argument -----------------------\n";
	display(accumulate(team, two));
	accumulate(accumulate(team, three), four);
	cout << "Display team:\n";
	display(team);
	cout << "use return value in assignment ----------------------\n";
	dup = accumulate(team, five);
	cout << "Displaying team:\n";
	display(team);
	cout << "Displaying dup after assignment:\n";
	display(dup);
	set_pc(four);
	cout << "ill-advised assignment ------------------------------\n";
	accumulate(dup, five);
	cout << "Displaying dup after ill-advised assignment:\n";
	display(dup);


	return 0;
}

void display(const free_throws & ft)
{
	using std::cout;
	cout << "Name: " << ft.name << "\n";
	cout << "Made: " << ft.made << "\n";
	cout << "Attempts: " << ft.attempts << "\n";
	cout << "Percent: " << ft.percent << "\n";
}

void set_pc(free_throws & ft)
{
	if (ft.attempts != 0)
		ft.percent = 100.0f * float(ft.made) / float(ft.attempts);
	else
		ft.percent = 0;
}

free_throws & accumulate(free_throws & target, const free_throws & source)
{
	target.attempts += source.attempts;
	target.made += source.made;
	set_pc(target);
	return target;
}
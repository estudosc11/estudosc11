// String_Introducing.cpp : Defines the entry point for the console application.
//

// str1.cpp -- introducing the string class
#include <iostream>
#include <string>

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

// using string constructors
int main()
{
	using namespace std;
	string one("Lottery Winner!"); // ctor #1
	cout << one << endl; // overloaded <<
	string two(20, '$'); // ctor #2
	cout << two << endl;
	string three(one); // ctor #3
	cout << three << endl;
	one += " Oops!"; // overloaded +=
	cout << one << endl;
	two = "Sorry! That was ";
	three[0] = 'P';
	string four; // ctor #4
	four = two + three; // overloaded +, =
	cout << four << endl;
	char alls [] = "All's well that ends well";
	string five(alls, 20); // ctor #5
	cout << five << "!\n";
	string six(alls + 6, alls + 10); // ctor #6
	cout << six << ", ";
	string seven(&five[6], &five[10]); // ctor #6 again
	cout << seven << "...\n";
	string eight(four, 7, 16); // ctor #7
	cout << eight << " in motion!" << endl;

	cout << "Meu teste\n";
	char teste [] = "meu teste!\n";
	char teste1[256];

	strncpy(teste1, "meu teste!\n", 20);

	string tst(teste + 4, teste + 10);
	
	cout << "result: " << tst << endl;

	while (1);
	return 0;
}

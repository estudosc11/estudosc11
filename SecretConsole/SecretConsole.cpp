// SecretConsole.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>

int _tmain(int argc, _TCHAR* argv[])
{
	using namespace std;
	int rats = 101;
	int & rodents = rats; // rodents is a reference
	cout << "rats = " << rats;
	cout << ", rodents = " << rodents << endl;
	cout << "rats address = " << &rats;
	cout << ", rodents address = " << &rodents << endl;

	int bunnies = 50;
	rodents = bunnies; // can we change the reference ?
	cout << "bunnies = " << bunnies;
	cout << ", rodents = " << rodents << endl;
	cout << "bunnnies address = " << &bunnies;
	cout << ", rodents address = " << &rodents << endl;

	return 0;
}


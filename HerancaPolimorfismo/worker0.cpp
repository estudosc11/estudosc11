// worker0.cpp -- working class methods

#include "worker0.h"
#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// worker methods
// must implement virtual destructor, even is pure

Worker::~Worker() {};

void Worker::Set()
{
	cout << "Enter worker's name:";
	getline(cin, fullname);
	cout << "Enter worker's ID:";
	cin >> id;
	while (cin.get() != '\n')
		continue;
}

void Worker::Show() const
{
	cout << "Name: " << fullname << "\n";
	cout << "EmployeID: " << id << "\n";
}

// Waiter methods
void Waiter::Set()
{
	Worker::Set();
	cout << "Enter waiter's panache rating: ";
	cin >> panache;
	while (cin.get() != '\n')
		continue;
}

void Waiter::Show() const
{
	cout << "Category waiter\n";
	Worker::Show();
	cout << "Panache rating: " << panache << "\n";
}

// Singer methods
char * Singer::pv[] = { "other", "alto", "contralto",
"Soprano", "bass", "baritone", "tenor" };

void Singer::Set()
{
	Worker::Set();
	cout << "Enter number for singer's vocal range:\n";
	int i;
	for (i = 0; i < VTypes; i++)
	{
		cout << i << ": " << pv[i] << " ";
		if (i % 4 == 3)
			cout << endl;
	}
	if (i % 4 != 0)
		cout << endl;
	while (cin >> voice && (voice < 0 || voice >= VTypes))
		cout << "Please enter a value >= 0 and < " << VTypes << endl;
	while (cin.get() != '\n')
		continue;
}

void Singer::Show() const
{
	cout << "Category Singer\n";
	Worker::Show();
	cout << "Vocal Range: " << pv[voice] << endl;
}




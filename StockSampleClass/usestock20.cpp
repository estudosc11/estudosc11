// using stock class
// compile with stock20.cpp

#include <iostream>
#include "stock20.h"

const int STKS = 4;

int main()
{
	// create an array of initialized objects
	Stock stocks[STKS] = {
		Stock("Mono", 12, 20.0),
		Stock("Boffo", 200, 2.0),
		Stock("Monolithic", 130, 3.25),
		Stock("Fleep", 60, 6.5)
	};
	std::cout << "Stock holdings:\n";
	int st;
	for (st = 0; st < STKS; st++)
		stocks[st].show();
	// set pointer to first element
	const Stock * top = &stocks[0];

	for (st = 1; st < STKS; st++)
		top = &top->topval(stocks[st]);

	// now top points to the most valuable holding
	std::cout << "\nMost valuable holding:\n";
	top->show();
	while (1);
	return 0;
}

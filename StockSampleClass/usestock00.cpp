// The client program
// compile with stock00.cpp

#include <iostream>
#include <string>
#include "stock00.h"
#include "Cliente.h"


int main()
{
	Stock fluffy_the_cat;
	fluffy_the_cat.acquire("Nanosmart", 20, 12.50);
	fluffy_the_cat.show();
	fluffy_the_cat.buy(15, 18.125);
	fluffy_the_cat.show();
	fluffy_the_cat.sell(400, 20.00);
	fluffy_the_cat.show();
	fluffy_the_cat.buy(300000, 40.125);
	fluffy_the_cat.show();
	fluffy_the_cat.sell(300000, 0.125);
	fluffy_the_cat.show();

	std::cout << "\n=================\n";
	std::string nome = "Silvio";
	Cliente cliente(nome);

	std::cout << "\n-----------------\n";
	std::cout << "nome alterado: " << nome;
	std::cout << "\n=================\n";
	char cnome[] = "Cesar\0";
	Cliente cliente1(cnome);
	std::cout << "\ncnome alterado? " << cnome;

	while (1);
	return 0;
}
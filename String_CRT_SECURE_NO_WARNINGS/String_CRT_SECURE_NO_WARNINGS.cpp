// String_CRT_SECURE_NO_WARNINGS.cpp : Defines the entry point for the console application.
//

/*
#include "stdafx.h"


int _tmain(int argc, _TCHAR* argv[])
{
	return 0;
}
*/
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <cstdlib>
#include <iostream>
#include <string>

int main()
{
	char * mens;
	mens = new char[45];

	strcpy(mens, "Teste\n");
	std::string mens_str(mens);

	printf("Teste - mens:%s\n", mens);
	printf("Teste - mens_str:%s\n", mens_str.c_str());  // passing c-style string
	std::cout << "Teste - mens_str:" << mens_str << std::endl;

	while (1);
}

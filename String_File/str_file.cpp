
// Listing 16.2 strfile.cpp
// read strings from a file

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

int main()
{
	using namespace std;
	ifstream fin;
	fin.open("tobuy.txt");
	if (fin.is_open() == false)
	{
		cerr << "can't open file. Bye!\n";
		
		while (1); // for view debug
		exit(EXIT_FAILURE);
	}

	string item;
	int count = 0;
	getline(fin, item, ':');
	while (fin) // while input is good
	{
		++count;
		cout << count << " : " << item << endl;
		getline(fin, item, ':');
	}

	cout << "Done!\n";

	fin.close();

	
	while (1); // for view debug
	return 0;


}
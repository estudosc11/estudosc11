// Listing 12.11 - queue.cpp
// Queue and custumer methods

#include "queue.h"
#include <iostream>
#include <cstdlib>
#include <sstream> // for ostringstream

// Queue methods
Queue::Queue(int qs) : qsize(qs)
{
	front = near = NULL; // or nullptr
	items = 0;
}

Queue::~Queue()
{
	Node * temp;
	while (front != NULL)  // while queue is not yet empty
	{
		temp = front; // save address of front item
		
		front = front->next; // reset pointer to next item
		//delete temp->item.buff;
		delete temp; // delete former front
	}

}

bool Queue::isempty() const
{
	return items == 0;
}

bool Queue::isfull() const
{
	return items == qsize;
}

int Queue::queuecount() const
{
	return items;
}

// Add item to queue
bool Queue::enqueue(const Item & item)
{
	if (isfull())
		return false;
	Node * add = new Node; // Create node
	// in failture, new throws std::bad_alloc exception
	add->item = item; // set node pointers
	add->next = NULL; // or nullptr

	items++;
	if (front == NULL)  // if queue is empty
		front = add; // place item at front
	else
		near->next = add; // else place at near
	near = add; // have near point to new node
	
	return true;

}

// Place front item into item variable and remove from queue
bool Queue::dequeue(Item & item)
{
	if (front == NULL)
		return false;
	item = front->item; // set to first item in queue
	items--;
	Node *temp = front; // save location of first item
	front = front->next; // reset front to next item
	delete temp; // delete former first item
	if (items == 0)
		near = NULL;
	
	return true;

}

// Place front item into item variable and remove from queue
bool Queue::find(Item & item)
{

	Node * temp;
	temp = front; // save address of front item
	while (temp != NULL)  // while queue is not yet empty
	{
		if (temp->item.seq == item.seq)
		{
			item = temp->item;
			item.contador = item.contador + 1;
			return true;
		}
		temp = temp->next; // save address of front item

	}

	return false;

}



// customer metod
// when is the time at which the customer arrives the arrivel time is set to when and
// the processing time set to a random value in the range 1-3
void Customer::set(long when)
{
	processtime = std::rand() % 3 + 1;
	arrive = when;
}

Customer::Customer()
{
	arrive = processtime = 0;
	seq = -1;
	contador = 0;
	buff = new char[20 + 1];
	//std::ostringstream out;
	//out << "ct: " << contador;
	//std::cout << out.str() << std::endl;
	
}

Customer::~Customer()
{
	delete[] buff;
}

Customer::Customer(const Customer & cus)
{
	buff = new char[20 + 1];
	//std::strcpy(buff, cus.buff);
	std::memcpy(buff, cus.buff, 20);
}

Customer & Customer::operator=(const Customer & cus)
{
	if (this == &cus)
		return *this;
	delete[] buff;
	buff = new char[20 + 1];
	//std::strcpy(label, rs.label);

	
	// acho que foi aqui meu erro
	std::sprintf(cus.buff, "seq:%d\n", cus.seq);
	std::cout << cus.buff << std::endl;
	
	std::memcpy(buff, cus.buff, 20);
	seq = cus.seq;
	contador = cus.contador;

	//std::memcpy(buff, out.str, 20);
	return *this;
}
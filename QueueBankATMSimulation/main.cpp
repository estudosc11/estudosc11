#include <iostream>
#include "queue.h"

int main()
{
	Queue queue(5);

	while (1)
	{
		Item item;
		for (int i = 0; i < 5; i++)
		{
			item.seq = i;
			queue.enqueue(item);
		}
		for (int i = 0; i < 5; i++)
		{
			item.seq = i;
			if (queue.find(item))
			{
				std::cout << "** seq:" << item.seq 
					<< ", buf:" << item.buff
					<< ", contador:" << item.contador << std::endl;
			}
		}
		for (int i = 0; i < 2; i++)
		{
			item.seq = i;
			if (queue.find(item))
			{
				std::cout << "** seq:" << item.seq
					<< ", buf:" << item.buff
					<< ", contador:" << item.contador << std::endl;
			}
		}
		for (int i = 0; i < 2; i++)
		{
			item.seq = i;
			if (queue.find(item))
			{
				std::cout << "** seq:" << item.seq
					<< ", buf:" << item.buff
					<< ", contador:" << item.contador << std::endl;
			}
		}

		for (int i = 0; i < 5; i++)
		{
			queue.dequeue(item);
			std::cout << item.seq << std::endl;
		}
	}

	while (1);
	return 0;
}
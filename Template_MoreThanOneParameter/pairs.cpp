// Listing 14.19 pairs.cpp
// Defining and using a Pair Template

#include <string>
#include <iostream>

template <class T1, class T2>
class Pair
{
private:
	T1 a;
	T2 b;
public:
	T1 & first();
	T2 & second();
	T1 first() const { return a;  }
	T2 second() const { return b;  }
	Pair(const T1 & aval, const T2 & bval) : a(aval), b(bval) {}
	Pair() {}
};

template <class T1, class T2>
T1 & Pair <T1, T2>::first()
{
	return a;
}

template <class T1, class T2>
T2 & Pair <T1, T2>::second()
{
	return b;
}

int main()
{
	using std::cout;
	using std::endl;
	using std::string;

	Pair<string, int> ratings[4] =
	{
		Pair<string, int>("The Pupled Duck", 5),
		Pair<string, int>("Jackie's Frisco Al Fresco", 4),
		Pair<string, int>("Cafe Souffle", 5),
		Pair<string, int>("Berthe's Eats", 3)
	};

	int joints = sizeof(ratings) / sizeof(Pair<string, int>);

	cout << "ratings:\tEatery\n";

	for (int i = 0; i < joints; i++)
		cout << ratings[i].first() << ":\t"
		<< ratings[i].second() << endl;

	cout << "Oops ! Revised rating>\n";

	ratings[3].first() = "Bertie's Fab Eats";
	ratings[3].second() = 6;

	cout << ratings[3].first() << ":\t"
		<< ratings[3].second() << endl;


	while (1);
	return 0;
}